const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const terser = require('gulp-terser');
const browsersync = require('browser-sync').create();
var gulp = require('gulp');

// Sass Task
function scssTask(){
  return src('src/scss/*.scss', { sourcemaps: true })
    .pipe(sass())
    .pipe(postcss([cssnano()]))
    .pipe(dest('dist/styles/', { sourcemaps: '.' }));
}

// JavaScript Task
function jsTask(){
  return src('src/js/*.js', { sourcemaps: true })
    .pipe(terser())
    .pipe(dest('dist/scripts/', { sourcemaps: '.' }));
}

//html task
function htmlTask(){
    return src('src/*.html')
    .pipe(gulp.dest('dist'))
}

// Browsersync Tasks
function browsersyncServe(cb){
  browsersync.init({
    server: {
      baseDir: 'dist'
    }
  });
  cb();
}

function browsersyncReload(cb){
  browsersync.reload();
  cb();
}

// Watch Task
function watchTask(){
  watch('src/*.html', series(htmlTask, browsersyncReload));
  watch(['src/scss/**/*.scss', 'src/js/**/*.js'], series(scssTask, jsTask, browsersyncReload));
}

// Default Gulp task
exports.default = series(
  htmlTask,
  scssTask,
  jsTask,
  browsersyncServe,
  watchTask
);