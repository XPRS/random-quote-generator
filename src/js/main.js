//selectors
const todoInput = document.querySelector('.todo__input');
const todoButton = document.querySelector('.todo__button');
const todosContainer = document.querySelector('.todos__contianer');
const todosList = document.querySelector('.todo__list');
const filter = document.querySelector('.todo__filter');
const copyBtn = document.querySelector('.copy');
const clearBtn = document.querySelector('.clear');
const todoActions= document.querySelector('.todo__actions');

// event listners
document.addEventListener('DOMContentLoaded', checkTodoListCount )
document.addEventListener('DOMContentLoaded', getTodos);
todoButton.addEventListener('click', addTodo);
todosList.addEventListener('click', deleteCheck);
filter.addEventListener('click',  filterTodos);
clearBtn.addEventListener('click', deleteList);

// FUNCTIONS
// check current todolist count
function checkTodoListCount() {
    let localTodos;
    if (localStorage.getItem('todos') === null) {
        localTodos = [];
        if (localTodos.length < 1) {
            clearBtn.classList.add('hidden');
            copyBtn.classList.add('hidden');
            filter.classList.add('disabled');
        }
    } else {
        localTodos = JSON.parse(localStorage.getItem('todos'));
        if (localTodos.length < 1) {
            clearBtn.classList.add('hidden');
            copyBtn.classList.add('hidden');
            filter.classList.add('disabled');
        } else if (localTodos.length > 1) {
            clearBtn.classList.remove('hidden');
            copyBtn.classList.remove('hidden');
            filter.classList.remove('disabled');
        }
    }
}

//copy list
function copyList() {
    let completed = document.querySelectorAll('div.todo');
    var list = [].slice.call(completed);
    var innertext = list.map(function(e) { return e.innerText; }).join("\n");

    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.setAttribute("id", "dummy_id");
    document.getElementById("dummy_id").value=innertext;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);

};



// delete list 
function deleteList() {
    let todos = todosList.children;
    while(todos.length > 0) {
        for (item of todos) {
            item.remove();
            localStorage.clear();    
        }
    }
    if (todosList.children.length < 1) {
        clearBtn.classList.add('hidden');
        copyBtn.classList.add('hidden');
        filter.classList.add('disabled');
    }
    if (todosList.children.length > 1) {
        clearBtn.classList.remove('hidden');
        copyBtn.classList.remove('hidden');
        filter.classList.remove('disabled');
    }
}

// add todo
function addTodo(e) {
    if (todoInput.value.length > 1) {
        //prevent default
        e.preventDefault();
        // create todo wrapper
        const todoItem = document.createElement("div");
        todoItem.classList.add('todo');

        // create text item
        const todoText = document.createElement('li');
        todoText.classList.add('todo__text');
        todoText.innerText = todoInput.value;
        todo = todoInput.value;
        todoItem.appendChild(todoText);

        // create completed button
        const doneButton = document.createElement('button');
        doneButton.classList.add('btn_done');
        doneButton.innerHTML = `<i class="fas fa-check"></i>`;
        todoItem.appendChild(doneButton);

        // create delete button
        const deleteButton = document.createElement('button');
        deleteButton.classList.add('btn_delete');
        deleteButton.innerHTML = `<i class="fas fa-times"></i>`;
        todoItem.appendChild(deleteButton);
        filterTodos(e);

        //add todo div to todo list
        todosList.appendChild(todoItem);

        //add todo to localstorage
        saveLocalTodos(todo);

        //clear tod value
        todoInput.value = "";

        //show hide copy btn
        if (todosList.children.length < 1) {
            clearBtn.classList.add('hidden');
            copyBtn.classList.add('hidden');
            filter.classList.add('disabled');
        }
        if (todosList.children.length >= 1) {
            clearBtn.classList.remove('hidden');
            copyBtn.classList.remove('hidden');
            filter.classList.remove('disabled');
        }
        copyBtn.addEventListener('click', copyList);
        clearBtn.addEventListener('click', deleteList);

    } else {
        e.preventDefault();
    }
}

//delete todo items
function deleteCheck(e) {
    const item = e.target;
    // delete
    if (item.classList[0] === "btn_delete") {
        const todo = item.parentElement;
        todo.classList.add('drop');
        removeLocal(todo);
        
        todo.addEventListener('transitionend', function() {
            todo.remove();
            if (todosList.children.length < 1) {
                clearBtn.classList.add('hidden');
                copyBtn.classList.add('hidden');
                filter.classList.add('disabled');
            }
            if (todosList.children.length > 1) {
                clearBtn.classList.remove('hidden');
                copyBtn.classList.remove('hidden');
                filter.classList.remove('disabled');
            }
    
        });
        
    } 
    // check
    if (item.classList[0] === "btn_done") {        
        const todo = item.parentElement;
        const checked = document.querySelector('.checked');

        if (todo.firstChild.classList.contains('checked')) {
            todo.firstChild.remove();
        } else {
            const iconContainer = document.createElement('div');
            iconContainer.classList.add('checked');
            

            todo.append(iconContainer);
            iconContainer.innerHTML = `<i class="fas fa-check"></i>`;

            todo.insertBefore(iconContainer,  todo.firstChild);
        }

        todo.classList.toggle("completed")
    }

    //message
    function updateMessage() {
        let completed = document.querySelectorAll('.completed');
        
        if (completed.length >= 2) {
            const heading = document.querySelector('h1');
            heading.innerText = "Slow and steady wins the race";
        } if (completed.length >= 4) {
            const heading = document.querySelector('h1');
            heading.innerText = "One task at a time";
        }
         if (completed.length < 2) {
            const heading = document.querySelector('h1');
            heading.innerText = "Stay calm and get it done.";
        }
    }
    updateMessage();
    
    
}

//filter todos
function filterTodos(e) {
    const todos = filter.children;
    const list = todosList.children;

    if (e.target.matches('.todo__button')) {
        for (let todo of todos) {
            todo.classList.remove('todo__option--active');
            if (todo.classList.contains('all')) {
                todo.classList.add('todo__option--active');
            }
        }

        for (let item of list) {
            if (item.matches('.todo.completed')) {
                item.style.display = 'flex';
                copyBtn.addEventListener('click', copyList)
                clearBtn.addEventListener('click', deleteList);
            } else {
                item.style.display = 'flex';
                copyBtn.addEventListener('click', copyList)
                clearBtn.addEventListener('click', deleteList);
            }
        } 

    } else {
        for (let todo of todos) {
            todo.classList.remove('todo__option--active');
            e.target.classList.add('todo__option--active');
    
            
            
            if (todo.matches('.todo.todo__option--active')) {
                for (let item of list) {
                    if (item.matches('.todo.completed')) {
                        item.style.display = 'none';
                    } else {
                        item.style.display = 'flex';
                        copyBtn.addEventListener('click', copyList)
                        clearBtn.addEventListener('click', deleteList);
                    }
                }
                
            } else if (todo.matches('.done.todo__option--active'))  {
                for (let item of list) {
                    if (item.matches('.todo.completed')) {
                        item.style.display = 'flex';
                        copyBtn.addEventListener('click', copyList)
                        clearBtn.addEventListener('click', deleteList);
                    } else {
                        item.style.display = 'none';
                    }
                } 
                
            } else if (todo.matches('.all.todo__option--active'))  {
                for (let item of list) {
                    if (item.matches('.todo.completed')) {
                        item.style.display = 'flex';
                        copyBtn.addEventListener('click', copyList)
                        clearBtn.addEventListener('click', deleteList);
                    } else {
                        item.style.display = 'flex';
                        copyBtn.addEventListener('click', copyList)
                        clearBtn.addEventListener('click', deleteList);
                    }
                } 
            }
    
            
        }
    
    }

    
}

//Add to local storage
function saveLocalTodos(todo) {
    let todos;
    if (localStorage.getItem('todos') === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }
    todos.push(todo);
    localStorage.setItem('todos', JSON.stringify(todos));
}

//get todos already created from local storage
function getTodos() {
    let todos;
    if (localStorage.getItem('todos') === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }

    //add todos from local storage to ui
    todos.forEach(function(todo) {
        // create todo wrapper
        const todoItem = document.createElement("div");
        todoItem.classList.add('todo');

        // create text item
        const todoText = document.createElement('li');
        todoText.classList.add('todo__text');
        todoText.innerText = todo;
        todoItem.appendChild(todoText);

        // create completed button
        const doneButton = document.createElement('button');
        doneButton.classList.add('btn_done');
        doneButton.innerHTML = `<i class="fas fa-check"></i>`;
        todoItem.appendChild(doneButton);

        // create delete button
        const deleteButton = document.createElement('button');
        deleteButton.classList.add('btn_delete');
        deleteButton.innerHTML = `<i class="fas fa-times"></i>`;
        todoItem.appendChild(deleteButton);

        //add todo div to todo list
        todosList.appendChild(todoItem);

        
    });

}


//remove todo item from local storage too
function removeLocal(todo) {
    let todos;
    if (localStorage.getItem('todos') === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }
    const todoIndex = todo.children[0].innerText;
    todos.splice(todos.indexOf(todoIndex), 1);
    localStorage.setItem('todos', JSON.stringify(todos));

}


//// GSAP
gsap.registerPlugin(ScrollTrigger);

gsap.to(todoActions, {
    scrollTrigger: {
        trigger:todoActions,
        endTrigger:"nothing",
        end:"none",
        start:'top top',
        toggleActions:"restart none none none",
        pin:true,
        pinSpacing:false,
        end: "bottom bottom"
    }  
})



